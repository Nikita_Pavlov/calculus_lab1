left = 0
right = 0.5
counter = 0

function y = given_func(x)
    y=exp(3*x)
endfunction

function y = integral_for_gf(x)
    y=exp(3*x)/3
endfunction

while (%t)
    rtype =  input("Enter type: left\right\middle ", "string")
    n = input("Enter N ")
    
    delta = (right-left)/n
    
    data = zeros(n)
    Xs=zeros(n+1)
    Ys=zeros(n+1)
    
    for k = 1:n
        Xs(k+1) = left+k*delta
    end
    Ys = given_func(Xs)
    
    is = integral_for_gf(right) - integral_for_gf(left)
    
    
    scf(counter)
    
    select rtype
        case "right" then
            Yi=Ys
        case "middle" then
            Yi = given_func(Xs-delta/2)
        case "left" then
            Yi = given_func(Xs-delta)
        else disp("No such type found")
    end
    
    s=sum(Yi)*delta
    for k = 2:n+1
        if n<100 then
            xrect(Xs(k)-delta,Yi(k),delta,Yi(k))
        else
            xfrect(Xs(k)-delta,Yi(k),delta,Yi(k))
        end
        gce().fill_mode="on";
        gce().foreground=color("black");
        gce().background=color(82, 203, 247);
    end
    
    plot2d(Xs,Ys,5)
    set(gca(),'data_bounds',[left,min(0,min(Ys)); right,max(max(Ys),0)+0.15])
    gcf().figure_size = [600,600]

    
    offset=(right-left)/45
    voffset=0.3
    xstring(left+offset,max(max(Ys),0)+0.15,rtype+' rectangles, n = '+string(n))
    gce().font_size = 3; 
    xstring(left+offset,max(max(Ys),0)+0.15-voffset,'integral sum: '+string(s))
    gce().font_size = 3;
    xstring(left+offset,max(max(Ys),0)+0.15-2*voffset,'integral sum found via Newton-Leibniz formula: '+string(is))
    gce().font_size = 3;
    xstring(left+offset,max(max(Ys),0)+0.15-3*voffset,'absolute error: '+string(s-is))
    gce().font_size = 3;
    xstring(left+offset,max(max(Ys),0)+0.15-4*voffset,'relative error: '+string(abs((s-is)/is)*100)+'%')
    gce().font_size = 3;

    counter=counter+1;
end


